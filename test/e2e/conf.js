/**!
 * This module derives from okta/okta-oidc-js
 * Added JUnit report to test.
 *
 * @author Leo Zuo <Leo.Zuo@flex.com>
 * @copyright 2019 FLEX LTD.
 * @license Apache-2.0
 *
 * Copyright (c) 2019 FLEX LTD.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * Original work from
 * https://github.com/okta/okta-oidc-js/
 * Copyright (c) 2017-Present, Okta, Inc. and/or its affiliates. All rights reserved.
 * The Okta software accompanied by this notice is provided pursuant to the Apache License, Version 2.0 (the "License.")
 *
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */

exports.config = {
  framework: 'jasmine',
  onPrepare: function() {
    require('jasmine-reporters');
    jasmine.getEnv().addReporter(
      new jasmine.JUnitXmlReporter('./reports/jasmine', true, true, 'junit', true)
    );
  },
  directConnect: true,
  specs: ['specs/*.js'],
  baseUrl: 'http://localhost:8080/',
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: [
        '--headless',
        '--disable-gpu',
        '--window-size=800,600',
        '--no-sandbox'
      ]
     }
  }
}
